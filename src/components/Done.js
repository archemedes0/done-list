// Done.js

import React, { useEffect, useState } from 'react';
import './Done.css';
import ReactDOM from "react-dom";
import VerticalProgress from "./VerticalProgress";


function Task({ task, index, completeTask, removeTask}){
    return (
        <div
            className="task"
        >
            {task.title}
            <div className="task-duration">{task.duration}</div>
            <button style={{background: "red"}} onClick={() => removeTask(index)}>x</button>
            
        </div>
    );
}

function Done() {
    const [tasksDone, setTasksDone] = useState(0);
    
    const [tasks, setTasks] = useState([
        {
            title: "Grab some Pizza",
            completed: true,
            duration: 1
        },
        {
            title: "Do your workout",
            completed: true,
            duration: 2
        },
        {
            title: "Hangout with friends",
            completed: true,
            duration: 0
        }
    ]);

    const addTask = newField => {
        const title = newField.title;
        const duration = newField.duration;
        if (title != "" && duration !=""){
        const newTasks = [... tasks, {title, completed: true, duration:duration}];
        setTasks(newTasks);
        }
        
    }

    const completeTask = index => {
        const newTasks = [...tasks];
        newTasks[index].completed = true;
        setTasks(newTasks);
    }

    const removeTask = index => {
        const newTasks = [...tasks];
        newTasks.splice(index, 1);
        setTasks(newTasks);
    }

    const hrsIn = tasks.filter(task => task.duration).reduce(function(prev, cur) {
        return parseFloat(prev) + parseFloat(cur.duration);
      },0);    

    useEffect(() => {
        setTasksDone(hrsIn);

    });

    return (
        <div className="done-container">

            <div className="center">
                <CreateTask addTask={addTask} />
            </div>

            <div className="header">"What is left undone today, tomorrow's work will never do."- Goethe</div> 
            <div className="header">Total hours today: {tasksDone}</div> 
            <div className="tasks" class="center">
                {tasks.map((task, index) =>(
                    <Task
                        task={task}
                        index={index}
                        key={index}
                        completeTask = {completeTask}
                        removeTask = {removeTask}
                        setDuration= {task.duration}
                        key={index}
                    />                    
                ))}
            </div> 
        
        <table className="center">
        <tr className="center">
            <td>
            <div className="App" class="center">
                    <VerticalProgress progress={tasksDone} />                          
            </div>
            </td>
            <td>
            <div className="App" class="center">
                    <VerticalProgress progress={tasksDone} />                          
            </div>
            </td>
            <td>
            <div className="App" class="center">
                    <VerticalProgress progress={tasksDone} />                          
            </div>
            </td>
            <td>
            <div className="App" class="center">
                    <VerticalProgress progress={tasksDone} />                          
            </div>
            </td>
            <td>
            <div className="App" class="center">
                    <VerticalProgress progress={tasksDone} />                          
            </div>
            </td>
            <td>
            <div className="App" class="center">
                    <VerticalProgress progress={tasksDone} />                          
            </div>
            </td>
            <td>
            <div className="App" class="center">
                    <VerticalProgress progress={tasksDone} />                          
            </div>
            </td>

        </tr> 
        </table>
        </div>
        
    );
}

function CreateTask ({ addTask}) {
    const initValues ={
        title: "",
        duration: "",
        
    };

    const [values, setValues ] = useState(initValues)

    const handleInputChange = e => {
        const {name, value} = e.target;

        setValues({
            ...values,
            [name]: value
        });
        

    };
    const handleSubmit = e => {
        e.preventDefault();
        if (!values) return;

        addTask(values);
        
    }

    return (
        <form >
          <table class="center-two" >
            <tr>
              <input
                type="text"
                className="center-two"
                name= "title"
                label="title"
                value={values.title}
                onChange={handleInputChange}
                placeholder="Add a new task"
              />
            
            
              <input 
               className="center-two"
                type ="number"
                name="duration"
                label="duration"
                value ={values.duration}
                onChange={handleInputChange}
                placeholder ="Duration"
              />
            
            <button type="submit" className="center-two" onClick={handleSubmit}> Submit </button>
            </tr>

          </table>
        </form>
    );

}

export default Done;