import React from "react";
import { Spring } from "react-spring";

const VerticalProgress = ({ progress }) => {
  return (
    <Spring from={{ hours: 0 }} to={{ hours: progress }}>
      {({ hours }) => (
        <div className="progress vertical">
          <div style={{ height: `${(progress/24)*100}%` }} className="progress-bar">
            <span className="sr-only">{`${progress}h`}</span>
          </div>
        </div>
      )}
    </Spring>
  );
};

export default VerticalProgress;