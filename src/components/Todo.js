// Todo.js

import React, { useEffect, useState } from 'react';
import './Todo.css';

function Task({ task, index, completeTask, removeTask}){
    return (
        <div
            className="task"
            style={{ textDecoration: task.completed ? "line-through" : ""}}
        >
            {task.title}
            <div className="task-duration">{task.duration}</div>
            <button style={{background: "red"}} onClick={() => removeTask(index)}>x</button>
            <button onClick={() => completeTask(index)}>Complete</button>
        </div>
    );
}
function Todo() {
    const [tasksRemaining, setTasksRemaining] = useState(0);
    const [tasks, setTasks, duration] = useState([
        {
            title: "Grab some Pizza",
            completed: true,
            duration: 1
        },
        {
            title: "Do your workout",
            completed: true,
            duration: 2
        },
        {
            title: "Hangout with friends",
            completed: false,
            duration: 0
        }
    ]);

    useEffect(() => {
        setTasksRemaining(tasks.filter(task => !task.completed).length)
    });

    const addTask = newField => {
        const title = newField.title;
        const duratio = newField.duration;
        const newTasks = [... tasks, {title, completed: false, duration:duratio}];
        setTasks(newTasks);
    }

    const completeTask = index => {
        const newTasks = [...tasks];
        newTasks[index].completed = true;
        setTasks(newTasks);
    }
    const setDuration = index => {
        const newTasks = [...tasks];
        newTasks[index].duration = {duration};
        setTasks(newTasks);
    }

    const removeTask = index => {
        const newTasks = [...tasks];
        newTasks.splice(index, 1);
        setTasks(newTasks);
    }

    return (
        <div className="todo-container">
            <div className="header">Carpe diem.</div> 
            <div className="header">What is left undone today, tomorrow's work will never do.- Goethe</div> 
            <div className="header">Pending tasks ({tasksRemaining})</div> 
            <div className="tasks">
                {tasks.map((task, index) =>(
                    <Task
                        task={task}
                        index={index}
                        key={index}
                        completeTask = {completeTask}
                        removeTask = {removeTask}
                        setDuration= {setDuration}
                        key={index}
                    />
                ))}
            </div>
            <div className="create-task">
                <CreateTask addTask={addTask} />
            </div>
        </div>
    );
}

function CreateTask ({ addTask}) {
    const initValues ={
        title: "Execution is what matters",
        duration: "0",
    };

    const [values, setValues ] = useState(initValues)

    const handleInputChange = e => {
        const {name, value} = e.target;

        setValues({
            ...values,
            [name]: value
        });

    };
    const handleSubmit = e => {
        e.preventDefault();
        if (!values) return;

        addTask(values);
        
    }

    return (
        <form >
          <div>
            <td>
              <input
                type="text"
                className="input"
                name= "title"
                label="title"
                value={values.title}
                onChange={handleInputChange}
                placeholder="Add a new task"
              />
            </td>
            <td>
              <input 
               className="input"
                type ="number"
                name="duration"
                label="duration"
                value ={values.duration}
                onChange={handleInputChange}
                placeholder ="Duration"
              />
            </td>
            <td><button type="submit" onClick={handleSubmit}> Submit </button></td>
            
          </div>
        </form>
    );

}

export default Todo;