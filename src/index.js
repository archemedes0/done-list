import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Done from './components/Done';
import './components/Done.css';

import reportWebVitals from './reportWebVitals';

import * as serviceWorker from './serviceWorkerRegistration.js';

const rootElement = document.getElementById("root");

ReactDOM.render(<Done />,rootElement);

serviceWorker.unregister();
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
